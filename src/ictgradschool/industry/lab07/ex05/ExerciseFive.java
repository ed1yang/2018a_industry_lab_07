package ictgradschool.industry.lab07.ex05;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)

        try{
            String s=getStringFromUser();
            printFirstLetter(s);
        }catch(ExceedMaxStringLengthException|InvalidWordException e){
            System.out.println(e.getMessage());
        }
    }






    // TODO Write some methods to help you.
    public String getStringFromUser()throws ExceedMaxStringLengthException{
        System.out.println("Enter a string of at most 100 characters:");
        String s= Keyboard.readInput();
        if(s.length()>100){
            throw new ExceedMaxStringLengthException("The maximum length of the string is 100 characters!");
        }else{
            return s;
        }

    }

    public void printFirstLetter(String s)throws InvalidWordException{
        int i;
        s+=" ";
        String answer="";
        do {
            char c = s.charAt(0);
            if (Character.getNumericValue(c) >= 10 && Character.getNumericValue(c) <= 35) {
                answer+=c + " ";
                i = s.indexOf(" ") + 1;
                s=s.substring(i);
            } else {
                throw new InvalidWordException("The input String contains invalid words.");
            }
        }while(s.indexOf(" ")!=-1);
        System.out.println("You entered: "+answer);



    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
