package ictgradschool.industry.lab07.ex05;

public class InvalidWordException extends Exception {
    public InvalidWordException(String message){
        super(message);
    }
}
