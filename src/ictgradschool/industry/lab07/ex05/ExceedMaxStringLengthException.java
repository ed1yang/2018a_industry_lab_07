package ictgradschool.industry.lab07.ex05;

public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException(String message){
        super(message);
    }
}
