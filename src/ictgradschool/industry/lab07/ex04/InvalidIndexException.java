package ictgradschool.industry.lab07.ex04;

public class InvalidIndexException extends Exception {
    public InvalidIndexException(String message){
        super(message);
    }
}
