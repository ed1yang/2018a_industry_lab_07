package ictgradschool.industry.lab07.ex04;

public class IndexTooLowException extends Exception {
    public IndexTooLowException(String message){
        super(message);
    }
}
